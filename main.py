# -*- coding: utf-8 -*-
import base64
import calendar
import csv
import datetime
import email
import gzip
import hashlib
import httplib2
import imaplib
import io
import json
import logging
import mimetypes
import operator
import os
from pprint import pprint
import random
import re
import cStringIO as StringIO
import shutil
import string
import subprocess
import sys
import tempfile
import threading
import time
import types
import urllib
import urlparse
import wave
import xml
from xml.etree import ElementTree as ET
import zipfile

import audiotools
from backports import lzma
from bs4 import BeautifulSoup, SoupStrainer
from config import Config
from Crypto.Cipher import AES as Aes
import dropbox
import dwolla
from facepy import GraphAPI
from feedgen.feed import FeedGenerator
import feedparser
import gnupg
import holidays
from icalendar import Calendar, Event
from icalendar.prop import vDDDTypes, vText
import json2html
from langid.langid import LanguageIdentifier, model
import magic
from markdown import markdown
import nltk
import oauth2client
import oauth2client.file
from oauth2client import client, tools
from oauth2client.client import SignedJwtAssertionCredentials
import pandas as pd
import praw
import pytz
import pyimgur
import qrcode
import redis
import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import HTTPError
from requests_oauthlib import OAuth1
import simplegist
import speech_recognition as sr
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lsa import LsaSummarizer
from sumy.utils import get_stop_words
from textblob import TextBlob
from textstat.textstat import textstat
from twilio.rest import TwilioRestClient
import web

urls = (
    '/', 'Help',
    '/airport/([A-Z]{3,4})/?', 'IATA',
    '/app/([^/]+/)?', 'Appstore',
    '/cloud/?', 'Cloud',
    '/code/([^/]+/)?', 'Github',
    '/cooking/?', 'Recipe',
    '/crypt/?', 'AES',
    '/db/?', 'Redis',
    '/dwolla/?', 'Dwolla',
    '/ebay/([^/]+)/?', 'EBay',
    '/elections/?', 'HuffPo',
    '/favicon.ico', 'RobotsTxt',
    # must include the '/' here, else FB will have a cow
    '/fb/', 'Facebook',
    '/feed/?', 'Feed',
    '/firebase/([^:]+):([^@]+)@([^/]+)//?', 'Firebase',
    '/fips/?', 'Fips',
    '/food/?', 'Diet',
    '/help/?', 'Help',
    '/holiday/?', 'Holiday',
    '/html/?', 'Html',
    '/imap/([^/]+)/([^/]+)/?', 'Imap',
    '/ip', 'IP',
    '/law/?', 'GovTrack',
    '/lorem/?', 'Lorem',
    '/maven/?', 'Maven',
    '/poi/?', 'Location',
    '/nasa/?', 'Apod',
    '/paste/?([^/]+)/?', 'Pastebin',
    '/pgp/?', 'PGP',
    '/qrcode/?', 'QRCode',
    '/quote/?', 'Stock',
    '/rand/?', 'Random',
    '/reddit(/[^/]*)?/?', 'Reddit',
    '/robots.txt', 'RobotsTxt',
    '/schedule/?', 'Today',
    '/seo/?', 'SEO',
    '/sms/?', 'Twilio',
    '/speech/?', 'SpeechSynthesis',
    '/soccer/?([^/]+)/?', 'Soccer',
    '/sql/?', 'SQL',
    '/summary/?', 'Summarize',
    '/tax/([0-9.]+)/([.0-9]+)/?', 'Sales',
    '/time/([^/]+)/([^/]+)/([0-9]+)/?', 'Time',
    '/track/([^/]+)/?', 'Package',
    '/tv/([^/]+)?', 'TV',
    '/tz/([+0-9]+)/?', 'PhoneTime',
    '/unit/([^/]+)/([^/]+)/([0-9.]+)?/?', 'Unit',
    '/verify/?', 'Verify',
    '/weather/([0-9]{5})/?', 'Weather',
    '/wikipedia/(.*)', 'Wikipedia',
    '/word/([A-Za-z]+)/?', 'English',
    '/worldbank/?', 'Worldbank',
    '/yelp/([^/]+)/?', 'Yelp',
    '/yodlee/?', 'Yodlee',
)

app = web.application(urls, globals())

OUR_TWILIO_NUMBER = '+14152124578'


def is_html():
    """ Consists of two checks, first the user_agent and then the check for the remote location, to discern whether we want html output or json """
    user_agent = web.ctx.env['HTTP_USER_AGENT']
    gui_user = not user_agent.startswith('curl/')
    loc = web.input().get('q', None)
    return gui_user and not loc


def log(classname):
    address = web.ctx.env.get('X-Real-IP')

    try:
        requests.post('http://logging.d8u.us/', data={'app_id': 'units', 'metadata': {'endpoint': classname, 'host': address}})
    except BaseException, e:
        logging.fatal(str(e))


def to_html(jsonobj):
    query_url = web.input()['q']
    if not query_url.startswith('http'):
        title_url = 'http://units.d8u.us{0}'.format(query_url)
    else:
        title_url = query_url
    ret = '''<!DOCTYPE html>
<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><meta charset="UTF-8"><!--Import Google Icon Font--><link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><!--Import materialize.css--><link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css" media="screen,projection"/><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css"><script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script><link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"><!--Let browser know website is optimized for mobile--><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Units Output of {0} as HTML</title></head><body><header><h1>Units Output of {0}</h1></header><main><table class="mdl-data-table mdl-js-table" style="table-layout: fixed; width:100%"><thead><tr>'''.format(title_url)

    if type(jsonobj) == types.DictType:
        headers = jsonobj.keys()
        for col in sorted(headers):
            ret = ret + '<th class="mdl-data-table__cell--non-numeric">{0}</th>'.format(col.title())
        ret = ret + '</tr><tr>'
        for col in sorted(headers):
            ret = ret + process_row(jsonobj[col], headers)
        ret = ret + '</tr></thead><tbody>'

    elif type(jsonobj) == types.ListType:

        headers = set()
        for row in jsonobj:
            [headers.add(r) for r in row.iterkeys()]
        headers = sorted(list(headers))
        for col in headers:
            ret = ret + '<th class="mdl-data-table__cell--non-numeric">{0}</th>'.format(col.title())
        ret = ret + '</tr></thead><tbody>'

        for row in jsonobj:
            # fill in row
            for h in headers:
                if h not in row:
                    row[h] = ''

            ret = ret + '<tr>'
            for val in headers:
                ret = ret + process_row(row[val], headers)

    ret = ret + '</tr></tbody></table></main><footer>Feedback to <i><a href="http://bitbucket.org/hd1/units/issues/new">me</a></i>, if you should encounter problems.<div class="copyright"><div class="container">Copyright &#169;{0} by Hasan Diwan</div></footer></body></html>'.format(datetime.datetime.now().year)
    return compression(ret)


def process_row(row, headers):
    width = round(100/len(headers))
    row = str(row).encode('utf-8', errors='xmlcharrefreplace')
    ret = '<td class="mdl-data-table__cell--non-numeric" width="{1}%"><pre style="word-wrap: break-word;white-space: pre;white-space: pre-wrap;white-space: pre-line;white-space: -pre-wrap;white-space: -o-pre-wrap;white-space: -moz-pre-wrap;white-space: -hp-pre-wrap;"><p class="flow-text">{0}</p></pre></td>'.format(row, width)
    ret = ret
    return ret

def compression(data):
    web.header('Access-Control-Allow-Origin',      '*')
    web.header('Access-Control-Allow-Credentials', 'true')
    web.header('Content-Type', 'application/json')
    ret = json.dumps(data, sort_keys=True, indent=2)

    user_agent = web.ctx.env['HTTP_USER_AGENT']

    if is_html():
        web.input()['compression'] = 1

    if 'compression' in web.input():
        web.header('Content-Encoding', 'gzip')
        gzip_buffer = StringIO.StringIO()
        gzip_file = gzip.GzipFile(mode='wb',
                                  fileobj=gzip_buffer)
        gzip_file.write(ret)
        gzip_file.close()
        ret = gzip_buffer.getvalue()
    return ret


class IP:
    def GET(self):
        log('IP')
        ret = None
        ctx = web.ctx
        environment = ctx.env
        headers = environment.keys()
        ip_key = 'HTTP_X_REAL_IP'
        ret = {'ip': environment[ip_key]}
        return compression(ret)


class Error(web.HTTPError):
    '''HTTP errors.'''

    headers = {'Content-Type': 'application/json'}

    def __init__(self, note, headers=None, status='404 Not Found'):
        message = json.dumps([{'error': note}])
        web.HTTPError.__init__(self, status, headers or self.headers,
                               unicode(message))


class Twilio:
    def GET(self):
        log('Twilio')
        client = TwilioRestClient(Config.TWILIO_USER, Config.TWILIO_PASSWORD)
        response = web.input()
        body = response['Body']
        to = response['From']

        request = body.split(' ')
        request_endpoint = request[0]

        try:
            request_element = request[1]
        except IndexError:
            ret = {'status': 'Error, no default element'}
            raise Error(compression(ret))

        if not request_endpoint.startswith('/'):
            request_endpoint = '/{0}'.format(request_endpoint)

        response_url = 'http://{0}{1}'.format(web.ctx.host, request_endpoint)

        response_object = requests.get(response_url).json()

        error_body = 'Unknown response element, valid ones are {0}'.format(response_object.keys())

        massaged_element = request_element
        resp = response_object.get(massaged_element, 'No such element')
        if resp == 'No such element':
            massaged_element = massaged_element.replace('_', ' ')
            resp = response_object.get(massaged_element, error_body)

        if type(resp) not in [types.ListType, types.DictType, types.TupleType]:
            message = client.messages.create(to=to, from_=OUR_TWILIO_NUMBER, body=resp)

        elif type(resp) is types.NoneType:
            message = client.messages.create(to=to, from_=OUR_TWILIO_NUMBER, body='Nonexistent key for {0}'.format(request_endpoint))

        else:  # collection
            for response in resp:
                message = client.messages.create(to=to, from_=OUR_TWILIO_NUMBER, body=str(response))

        ret = {'status': 'OK'}

        return compression(ret)

    def POST(self):
        URL = 'https://api.twilio.com/2010-04-01/Accounts/AC267f658c19df221d076aaa5ebbb52928/SMS/Messages.json'
        params = {'From': OUR_TWILIO_NUMBER}
        auth = ('AC267f658c19df221d076aaa5ebbb52928', '4b0fbc8dabaf3364ab5547cf6971e82f')

        response = requests.get(URL, params=params, auth=auth).json()
        messages = response['sms_messages']

        ret = StringIO.StringIO()
        writer = csv.writer(ret)
        writer.writerow(messages[0].keys())
        for msg in messages:
            writer.writerow(msg.values())
        web.header('Content-Type', 'text/csv')
        return compression(ret.getvalue())


class SQL: 
    def GET(self):
        ret = []
        params = web.input()
        url = params.get('url', None)

        if url is None:
            raise Error('url parameter must point to a database')

        parsed_url = urlparse.urlparse(url)

        if parsed_url.scheme == 'sqlite3':
            import sqlite3
            path = parsed_url.path

            if path is None:
                path = ':memory:'

            conn = sqlite3.connect(path)
        elif parsed_url.scheme == 'pg':
            import pg8000.dbapi
            username = params.get('user', '')
            password = params.get('password', '')
            conn = pg8000.dbapi.connect(user=username, host=parsed_url.netloc, database=parsed_url.path, password=password)
        
        query = params.get('query', None)
        if query is None:
            raise Error('query parameter must be specified as full, valid SQL query')

        cursor = conn.cursor()
        cursor.execute(query)

        rows = cursor.fetchall()

        if rows is None:
            raise Error('Problem fetching results, statement failed')
        for row in rows:
            stanza = {}
            for index, column in enumerate(row):
                stanza[index] = row[index]
            ret.append(stanza)

        return compression(ret)


class RobotsTxt:
    def GET(self):
        context = web.ctx
        requested = context.homepath
        if requested == 'robots.txt':
            ret = '''User-agent: *
Disallow: /
'''
            return ret
        elif requested == 'favicon.ico':
            ret = 'meh!'
            return ret


class Firebase:
    def GET(self, user, password, hostname):
        params = web.input()

        real_host = 'https://{0}.firebaseio.com/'.format(hostname)
        endpoint = params.get('resource', None)

        if endpoint is None:
            raise Error('Endpoint required')

        if not endpoint.endswith('.json'):
            endpoint = '{0}.json'.format(endpoint)

        if endpoint.startswith('/'):
            endpoint = endpoint[1:]

        url = '{0}{1}'.format(real_host, endpoint)
        contents = requests.get(url).content

        return compression(contents)


class Cloud:
    def GET(self):
        log('Cloud')
        params = web.input()
        service = params.get('service', 'dropbox')
        if not service:
            ret = Error('Service name and service-specific parameters required', status=404)
        if service.lower() == 'dropbox':
            ret = self.Dropbox(params)
        elif service.lower() == 'google':
            ret = self.Platypus(params)
        elif sevice.lower() == 'microsoft':
            ret = self.OneDrive(params)
        elif service.lower() == 'box':
            ret = self.Box(params)
        try:
            ret = compression(ret)
        except UnicodeDecodeError, e:
            pass
        except MemoryError, e:
            raise Error(message=str(e), status=413)
        return ret

    def Box(self, params):
        ret = {}
        headers = {'Accept-Encoding': 'gzip, deflate'}
        code = params.get('auth', None)
        BOX_REDIRECT_URI = 'http://units.d8u.us/cloud?code={0}&service=box'
        authorize_url = 'https://app.box.com/api/oauth2/authorize?response_type=code&client_id={0}&redirect_uri={1}&state=security_token%3DKnhMJatFipTAnM0nHlZA'.format(Config.BOX_API_KEY, BOX_REDIRECT_URI)
        if code is None:
            message = """1. Go to {0}; Click allow; Call this URL again with the code as the auth parameter.""".format(authorize_url)
            value = {'error': message}
            return value
        post_params = {}
        post_params['code'] = code
        post_params['client_id'] = Config.BOX_API_KEY
        post_params['client_secret'] = Config.BOX_CLIENT_SECRET
        post_params['grant_type'] = 'authorization_code'

        web_response = requests.post('https://api.box.com/oauth2/token', params=post_params)
        response = web_response.json()

        ACCESS_TOKEN = response['access_token']
        REFRESH_TOKEN = response['refresh_token']
        header = {'Authorization': 'Bearer {0}'.format(ACCESS_TOKEN)}

        op = params.get('op', 'list')
        if op == 'retrieve':
            file_id = params.get('fileid')
            URL = 'https://api.box.com/2.0/files/{0}/content'.format(file_id)
            web_response = requests.get(URL, header=header, verify=False, stream=True)
            response = web.content
            ret['data'] = response

        elif op == 'list':
            folder = params.get('path', '0')
            URL = 'https://api.box.com/2.0/folders/{0}/items'.format(folder)
            web_response = requests.get(URL, header=header, verify=False)
            response = web_response.json()

            items = response['total_count']
            entries = response['entries']
            for i in xrange(0, items):
                entry = entries[i]
                type_of_entry = entry['type']
                name = entry['name']
                entry_id = entry['id']
                web_response = requests.get('https://api.box.com/2.0/files/{0}'.format(entry_id), header=header, verify=False)
                response = web_response.json()

                if type_of_entry == 'folder':
                    name = '{0}/'.format(name)

                item = {}
                item['name'] = name
                item['id'] = entry_id
                ret.append(item)
        return compression(ret)

    def dropbox(self, params):
        flow = dropbox.client.DropboxOAuth2FlowNoRedirect(Config.DROPBOX_KEY, Config.DROPBOX_SECRET)
        authorize_url = flow.start()
        code = params.get('auth', None)
        if code is None:
            message = """1. Go to {0}; Click allow; Call this URL again with the code as the auth parameter.""".format(authorize_url)
            value = {'error': message}
            return compression(value)

        try:
            access_token, user_id = flow.finish(code)
        except:
            return {'error': 'Expired token, get a new one by removing the auth parameter from the URL ({0}) and following the instructions provided.'.format(authorize_url)}

        client = dropbox.client.DropboxClient(access_token)
        
        path = params.get("path", '/')
        folder_metadata = client.metadata(path)
        ret = []
        if 'contents' in folder_metadata:
            contents = folder_metadata['contents']
            
            for content in contents:
                file_info = {}
                file_info['rootpath'] = folder_metadata['path']
                file_info['path'] = content['path']
                is_shared = 'shared_folder' in content
                file_info['shared?'] = is_shared
                ret.append(file_info)

        else: # single file, display it
            with client.get_file(path) as fin:
                out = io.BytesIO()
                data = fin.read()
                out.write(data)
                ret = out.getvalue()
                web.header('Content-Type: octet/stream')

        return compression(ret)


class QRCode:
    def GET(self):
        log('QRcode')
        ret = {}
        PATH = ''
        
        # default to random string
        default_string = Random().random_string()['string']

        string_to_encode = web.input().get('data', default_string)
        if not self.is_url(string_to_encode):
            ret['string'] = string_to_encode
            img = qrcode.make(string_to_encode)
            out = tempfile.NamedTemporaryFile(prefix='qrcode', suffix='.png', delete=False)
            img.save(out)
            out.close()
            PATH = out.name
            im = pyimgur.Imgur(Config.IMGUR_API_KEY)
            # upload to imgur 
            uploaded_image = im.upload_image(PATH, title='Uploaded using units(Tm)')
            ret['link'] = uploaded_image.link_big_square
            os.unlink(PATH)

        else:
            ret['url'] = string_to_encode
            zxing_params = {'u': ret['url']}
            data = requests.get('http://zxing.org/w/decode', params=zxing_params).text
            so = BeautifulSoup(data, 'lxml')
            tbl = so.find('table', {'id': 'result'})
            table_cell = tbl.find('pre')
            ret['data'] = table_cell.text
        return compression(ret)

    def qr_handler(self, proc, image, closure):
        for symbol in image:
            if not symbol.count:
                self.data = symbol.data

    def is_url(self, candidate_url):
        try:
            parsed = urlparse.urlparse(candidate_url)
            return parsed.scheme == 'http' and parsed.netloc.find('imgur.com') > -1
        except:
            pass
        return False


class Html:
    def GET(self):
        if not web.input().get('q'):
            raise web.seeother('/')

        json_url = web.input().get('q')
        if not json_url:
            raise Error('url to render not found')
        if json_url.startswith('//'):
            json_url = 'http:{0}'.format(json_url)

        if not json_url.startswith('http'):
            json_url = 'http://{1}:{2}/{0}'.format(json_url, web.ctx.environ['SERVER_NAME'], web.ctx.environ['SERVER_PORT'])
        try:
            jsonobj = requests.get(json_url).json()
        except BaseException, e:
            jsonobj = {'error': str(e)}
            return compression(to_html(jsonobj))
        if jsonobj is None:
            jsonobj = {'error': "A garden-variety error just happened and we couldn't parse {0}".format(json_url)}
        return compression(to_html(jsonobj))


class Lorem:
    def GET(self):
        params = web.input()
        word_length = params.get('length', 5)
        sentence_length = params.get('phrases', 1)

        response = requests.get('https://baconipsum.com/api/?type=meat-and-filler&sentences={0}&start-with-lorem=1'.format(word_length, sentence_length)).json()
        return compression({'response': response})


class SpeechSynthesis:
    def GET(self):
        ret = {}
        self.params = web.input()
        location = self.params.get('url')
        r = sr.Recognizer()

        web_response = requests.get(location, stream=True)
        
        with tempfile.NamedTemporaryFile(prefix=str(time.time()), suffix='.wav', delete=False) as WAV_FILE:
            WAV_FILE.write(web_response.content)
                
        ret['google'] = self.google(WAV_FILE.name)
        ret['url'] = location
        os.unlink(WAV_FILE.name)
        return compression(ret)

    def get_rate(self, filename):
        audio_file = wave.open(filename,'rb')
        rate = audio_file.getframerate()
        return rate

    def google(self, audio_data):
        flac_destination = tempfile.NamedTemporaryFile(prefix=str(time.time()), suffix='.flac')
        flac = audiotools.open(audio_data).convert(flac_destination.name, audiotools.FlacAudio)
        logging.info('Converting input file to flac: {0}'.format(flac))
        post_file = {'file':open(audio_data, 'rb')}
        sample_rate = self.get_rate(audio_data)
        headers = {'Content-Type': 'audio/x-flac; rate={0}'.format(sample_rate) }
        logging.debug(headers)
        my_params = {}
        my_params['key']  = Config.GOOGLE_SPEECH_RECOGNITION_API_KEY
        my_params['lang'] = self.params.get('language', 'en_US')
        r = requests.post('http://www.google.com/speech-api/v2/recognize', params=my_params, files=post_file, headers=headers)
        result = r.json()
        return result['result']
        
        
class Diet:
    def GET(self):
        log("/food")

        ret = []
        food_key = web.input()['food']

        results_format = 'json'
        serp = self.search()

        for item in serp:
            ndb = item['ndb']
            item_result = self.nutrient_report(ndb)
            for i in item_result:
                ret.append(i) 

        return compression(ret)

    # the search endpoint gets the NDB code and the proper title for the query and returns them as a list
    def search(self):
        ret = []

        food_key = web.input()['food']
        results_format = 'json'

        params = {'q': food_key, 'format': results_format, 'api_key': Config.DATA_GOV_API_KEY}
        headers = {'Content-Type': 'application/json'}
        results = requests.get('http://api.nal.usda.gov/ndb/search/', params=params, headers=headers).json()['list']

        for result in results['item']:
            item = {}
            item['name'] = result['name']
            item['ndb'] = result['ndbno']
            
            ret.append(item)
        
        return ret

    def nutrient_report(self, ndbid):
        ret = []

        results_format = 'json'
        params = {'ndbno': ndbid, 'format': results_format, 'api_key': Config.DATA_GOV_API_KEY, 'type': 'f'}
        headers = {'Content-Type': 'application/json'}
        results = requests.get('http://api.nal.usda.gov/ndb/reports/', params=params, headers=headers).json()
        report = results['report']
        food = report['food']
        nutrients = food['nutrients']
        for nutrient in nutrients:
            result = {}
            result['ndb_food_number'] = food['ndbno']
            result['manu'] = food.get('manu', None)
            result['name'] = nutrient['name']
            measures = nutrient['measures']
            for m in measures:
                result['serving_name'] = m['label']
                result['weight_grams'] = m['value']
            ret.append(result)

        return ret


class Today:
    def GET(self, today=datetime.datetime.today()):
        log('/today/{0}'.format(today))
        DATE_FORMAT = '%Y%m%dT%H%M%S'

        ret = []
        url_to_ics = web.input().get('s', None)
        if not url_to_ics:
            return compression({'error': 'Missing url to ics, specify it using the "s" get parameter'})

        calendar_data = requests.get(url_to_ics).content
        cal = Calendar.from_ical(calendar_data)
        string_for_today = today.strftime('%Y%m%d')
        for component in cal.walk():
            dates = component.get('dtstart')
            try:
                if component.get('dtstart').to_ical().startswith(string_for_today):
                    event = {}
                    summary = component.get('summary')
                    dtstart = component.get('dtstart')
                    dtend = component.get('dtend')

                    title = vText.from_ical(summary)
                    start_time = vDDDTypes.from_ical(dtstart)
                    if dtend:
                        end_time = vDDDTypes.from_ical(dtend)

                    event['title'] = vText.from_ical(title)
                    event['starts'] = start_time.strftime('%c')
                    if dtend:
                        event['ends'] = end_time.strftime('%c')
                    ret.append(event)
            except AttributeError:
                continue
        return compression(ret)


class HuffPo:
    def GET(self):
        log('HuffPo')
        ret = []
        state = web.input().get('state', 'US')
        params = {'showall': 'true', 'state': state}
        response = requests.get('http://elections.huffingtonpost.com/pollster/api/polls', params=params).json()
        for poll in response:
            this_poll = {}
            this_poll['source'] = poll['source']
            this_poll['conductor'] = ', '.join(p['name'] for p in poll['survey_houses'])
            this_poll['sponsor(s)'] = ', '.join([p['name'] for p in poll['sponsors']])
            this_poll['state'] = state

            this_poll['start date'] = poll['start_date']
            this_poll['end date'] = poll['end_date']

            for question in xrange(0, len(poll['questions'])):
                this_question = poll['questions'][question]
                this_poll['question{0}'.format(question)] = this_question['name'].title()
                for population in xrange(0, len(this_question['subpopulations'])):
                    this_population = this_question['subpopulations'][population]
                    options = this_population['responses']
                    for option in xrange(0, len(options)):
                        this_option = options[option]
                        this_poll['choice({0}-{1}-{2})'.format(question, population, option)] = '{0} {1}'.format(this_option['first_name'], this_option['last_name'])
                        this_poll['rating({0}-{1}-{2})'.format(question, population, option)] = this_option['value']
            ret.append(this_poll)

        return compression(ret)


class Pastebin:
    FILE = '/var/tmp/pastes.json.xz'

    def load_pastes(self):
        with lzma.LZMAFile(Pastebin.FILE) as fin:
            self.pastes = json.load(fin)


    def GET(self, pasteid, are_we_testing=False):
        log('pastebin')
        pastes = {}
        try:
            self.load_pastes()
            pastes = self.pastes
        except IOError, e:
            ret = {'id': 'Paste {0} already expired'.format(pasteid)}
            return compression(ret)

        are_we_deleting = (web.input().get('op') == 'cleanup')

        for paste in pastes:
            expiration = pastes['expiration_time']
            if expiration < now:
                if are_we_deleting:
                    del pastes[paste]
        if are_we_deleting:
            return None

        retrieved = pastes[pasteid]
        goes_away_at = retrieved['expiration']
        content = retrieved['data']
        testing = web.input().get('testing') != False

        web.header('Content-Type', 'application/json')
        ret = {'data': content, 'id': pasteid, 'expires_at': goes_away_at}
        return compression(ret)

    def POST(self, get_param=None):
        newest_paste = {}

        body = web.data()
        bodyobj = json.loads(body)
        newest_paste['data'] = bodyobj['content']

        now = datetime.datetime.now()
        expiration_time = now + datetime.timedelta(hours=1)

        try:
            pastes = self.load_pastes()
        except IOError, e:
            logging.fatal('Expected, if this is the first run, else report to Hasan: {0}'.format(str(e)))
            pastes = []

        newest_paste['expiration'] = expiration_time.strftime('%s')
        id = hashlib.sha224(newest_paste['data']).hexdigest()

        new_paste = {}
        new_paste[id] = newest_paste
        pastes.append(newest_paste)

        with lzma.LZMAFile(Pastebin.FILE, 'a') as fout:
            try:
                json.dump(pastes, fout)
                newest_paste['saved'] = True
            except:
                newest_paste['saved'] = False

        return compression(newest_paste)

    def should_expire(self, pastes):
        now = long(time.time())
        ret = []
        for p in pastes:
            this_paste = pastes[p]
            expiration = long(this_paste['expiration'])
            if expiration < now:
                ret.append(this_paste['id'])
        return ret

class Redis:
    def __len__(self):
        return int(self.r.dbsize()) or 0

    def GET(self):
        params = web.input()
        ret = []

        hostname = params.get('host', 'pub-redis-10058.us-east-1-3.7.ec2.redislabs.com')
        port = params.get('port', '10058')
        db = params.get('db', '0')
            
        pool = redis.ConnectionPool(host=hostname, port=port, db=db, password=Config.REDIS_PASSWORD)
        self.r = redis.Redis(connection_pool=pool)
        
        if 'op' in params and params.get('op').lower() == 'flush':
            self.r.flushall()
            pool.disconnect()
            return compression({'Status': 'Success'})

        next_row = len(self)+1

        for k in params.iterkeys():
            if k in ['op', 'port', 'db', 'host']:
                continue
            self.r.hset(next_row, k, params[k])

        for row in xrange(0, next_row):
            element = str(row)
            stanza = self.r.hgetall(element)
            ret.append(stanza)

        pool.disconnect()

        return compression(ret)
        

class GovTrack:
    def GET(self):
        log('govtrack')
        ret = []
        params = web.input()
        limit = params.get('limit', 6000)
        if limit > 6000:
            ret = {'error': 'Result sets longer than 6000 are not supported by this endpoint.', 'request uri': web.ctx.path}
            return compression(ret)
        # https://www.govtrack.us/api/v2/vote?created__gt=2013-01-01T00:00:00
        today = datetime.date.today()
        url_formatted_today = today.strftime('%Y-01-01')
        created__gt = '{0}T00:00:00'.format(url_formatted_today)
        date_desired = params.get('date', created__gt)
        url_params = {'created__gt': created__gt, 'limit': 6000}
        response = requests.get("https://www.govtrack.us/api/v2/vote", params=url_params).json()
        try:
            bills = response['objects']
            for bill_order in range(0, len(bills)):
                bill = bills[bill_order]
                item = {}
                if not is_html():
                    item['link'] = bill['link']
                    item['title'] = bill['question']
                else:
                    item['title'] = '<a href="{0}">{1}</a>'.format(bill['link'], bill['question'])
                    if bill['result'] is not None:
                        if bill['result'] == 'Passed':
                            item['victor'] = '&#128024;'
                        elif bill['result'] == 'Failed':
                            item['victor'] = '<img src="https://upload.wikimedia.org/wikipedia/en/e/e3/DemocraticLogo.png">'
                item['created'] = bill['created']
                item['id'] = bill['id']
                if 'sponsor' in bill:
                    member_id = bill['sponsor']
                    member = requests.get('https://www.govtrack.us/api/v2/person/{0}'.format(member_id)).json()
                    name = member['name'][:member['name'].rindex('[')]
                    party = name[name.index('[')+1:name.rindex(']')]
                    item['sponsor'] = name
                    item['party'] = party

                ret.append(item)
        except BaseException, e:
            raise Error(compression({'Error': str(e), 'uri': web.ctx.path}))
        return compression(ret)


class Appstore:
    def droid(self, name):
        ret = {'Droid unsupported'}
        return ret

    def itunes(self, name):
        ret = []
        escaped_name = urllib.quote_plus(name)
        response = requests.get('https://itunes.apple.com/search?term={0}&media=software'.format(escaped_name)).json()
        results = response['results']

        for app in results:
            item = {}
            item['platform'] = 'iOS'
            item['query'] = name
            item['applink'] = app['trackViewUrl']
            item['category'] = ', '.join(app['genres'])
            item['rating'] = app['averageUserRating']
            item['name'] = app['trackCensoredName']
            item['price'] = app['price']
            item['description'] = app['description']
            item['created'] = app['releaseDate']
            ret.append(item)
        return ret

    def GET(self, name):
        log('Appstore')
        ret = []
        # ret.extend(self.droid(name))
        ret.extend(self.itunes(name))
        return compression(ret)


class TV:
    def GET(self, show):
        log('TV {0}'.format(show))
        show_escaped = urllib.quote_plus(show)
        apple_info = requests.get('https://itunes.apple.com/search?term={0}'.format(show_escaped)).json()
        try:
            tv_episodes = [a['results'] for a in apple_info['results'] if a['artistViewUrl'].find('tv-show') > -1]
            tv_episodes_sorted_by_date = sorted(tv_episodes, key=self.sort_key)
            latest_episode = tv_episodes_sorted_by_date[0]
        except KeyError, e:
            latest_episode = {}
            latest_episode['trackCensoredName'] = latest_episode['longDescription'] = latest_episode['releaseDate'] = 'Not available on iTunes'

        fp = feedparser.parse('https://kat.cr/tv/?rss=1')
        torrents = {}
        for torrent in fp.entries:
            ret = {}
            name = torrent['title']
            matching_name = name.lower().encode('utf-8')
            matching_request = show.lower()
            if matching_name.find(matching_request) != -1:
                torrent_data = len(torrents)+1
                if is_html():
                    ret['torrent {0} name'.format(torrent_data)] = '<a href="{1}">{0}</a>'.format(torrent['link'], torrent['name'])
                    del(torrent['name'])
                else:
                    ret['torrent {0} name'.format(torrent_data)] = name
                ret['torrent {0} magnet'.format(torrent_data)] = torrent['torrent_magneturi']
                ret['torrent {0} date'.format(torrent_data)] = torrent['published']
                if 'trackHdPrice' not in latest_episode:
                    ret['itunes price'] = latest_episode.get('trackPrice')
                    ret['itunes format'] = 'SD'
                else:
                    ret['itunes price'] = latest_episode.get('trackHdPrice')
                    ret['itunes format'] = 'HD'
                if not is_html():
                    iet['torrent {0} link'.format(torrent_data)] = torrent['link']

            ret['name'] = latest_episode['trackCensoredName']
            ret['episode description'] = latest_episode['longDescription']
            ret['air date'] = latest_episode['releaseDate']
            if ret['air date'] > torrent['publishehd']:
                continue
            if any([k.startswith('torrent') for k in ret.iterkeys()]):
                torrents.append(ret)
        return compression(torrents)

    def sort_key(self, item):
        time_key = item['releaseDate']
        timezone_less = time_key[:-1]
        timeobj = datetime.datetime.strptime(timezone_less, '%Y-%m-%dT%H:%M:%S')
        return timeobj

    def POST(self, wsdl=None):
        WSDL_URL = 'http://data.titantv.com/dataservice.asmx?WSDL'
        USERNAME = Config.TITAN_USER
        PASSWORD = Config.TITAN_PASSWORD
        if wsdl is None:
            wsdl = WSDL_URL
        endpoint = web.input().get('proc', None)
        if not endpoint:
            client = requests.get('{0}'.format(Config.TITAN_HOST), params = {'url': WSDL_URL}).json()
        else:
            client = requests.get('{0}/{1}'.format(Config.TITAN_HOST, endpoint)).json()

        return compression(client)


class English(xml.sax.handler.ContentHandler):
    THESAURUS_SOURCE = 'Thesaurus service provided by words.bighugelabs.com'

    def m_w(self, word):
        API_KEY = Config.MERRIAM_WEBSTER_API_KEY
        self.definitions = []
        result_ = requests.get('http://www.dictionaryapi.com/api/references/collegiate/xml/{0}?key={1}'.format(word, API_KEY))
        logging.fatal('M-W status code is {0}'.format(result_.status_code))
        if result_.status_code != requests.codes.ok:
            lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()
            root_word = lemmatizer.lemmatize(word, 'v')
            word = root_word
            result_ = requests.get('http://www.dictionaryapi.com/api/references/collegiate/xml/{0}?key={1}'.format(word, API_KEY))
            if result_.status_code != requests.codes.ok:
                return None

        result = result_.content
        stream = StringIO.StringIO(result)
        parser = xml.sax.make_parser()
        parser.setContentHandler(self)
        parser.parse(stream)

        ret = "; ".join(self.definitions)
        ret = ret.replace(':','').strip()
        return ret

    def startElement(self, name, attrs):
        self.element = name

    def characters(self, chars):
        if self.element == 'dt':
            if not chars.isspace():
                self.definitions.append(chars[1:])

    def GET(self, word):
        log('lookup for {0}'.format(word))
        self.word = word
        words = requests.get('https://raw.githubusercontent.com/adambom/dictionary/master/dictionary.json').json()
        self.ret = ret = {}
        key = word.upper()
        try:
            ret['definition'] = words[key]
        except BaseException, e:
            #definition = self.m_w(word)
            definition = None
            if definition:
                ret['definition'] = definition
            ret['source'] = "Merriam-Webster's Collegiate Dictionary with Audio"
        ret['word'] = word

        synonyms_ = requests.get('http://words.bighugelabs.com/api/2/aa77968939cdc6bf87b9682f1a58db21/{0}/json'.format(word))
        if synonyms_.status_code == 200:
            synonyms = synonyms_.json()
            ret['synonyms'] = ret['antonyms'] = ''
            for synonym in synonyms.values():
                if 'ant' in synonym:
                    ret['antonyms'] = ret['antonyms'] + ', '.join(synonym['ant'])
                if 'syn' in synonym:
                    ret['synonyms'] = ret['synonyms'] + ', '.join(synonym['syn'])

        urbandictionary_ = requests.get('http://api.urbandictionary.com/v0/define', params={'term':word})
        if urbandictionary_.status_code == 200:
            urbandictionary = urbandictionary_.json()

            if urbandictionary['result_type'] == 'exact':
                definitions = urbandictionary['list']

                for stanza in definitions:
                    stanza['vote'] = int(stanza['thumbs_up'])-int(stanza['thumbs_down'])

                key = operator.itemgetter('vote')
                sorted_definitions = sorted(definitions, key=key)
                best_definition = sorted_definitions[0]
                ret['slang'] = best_definition['definition'].replace('\r\n', ' ')

        ret['source'] = 'Thesaurus service provided by words.bighugelabs.com. Slang service provided by urbandictionary.com'
        return compression(ret)


class Soccer:
    def GET(self, team=''):
        log('soccer')
        self.team = team
        ret = self.team_standings(team)
        return compression(ret)

    def team_standings(self, desired_team):
        urls = {
            'La Liga': 'http://www.goal.com/en/tables/primera-divisi%C3%B3n/7',
            'Bundesliga': 'http://www.goal.com/en/tables/bundesliga/9?ICID=SP_TN_112',
            'Premier League': 'http://www.goal.com/en/tables/premier-league/8?ICID=TA',
            'Serie A': 'http://www.goal.com/en/tables/serie-a/13?ICID=SP_TN_114',
            'Ligue 1': 'http://www.goal.com/en/tables/ligue-1/16?ICID=SP_TN_114',
        }
        teams = []
        for league in urls.iterkeys():
            url = urls[league]
            data = requests.get(url).text
            so = BeautifulSoup(data, 'lxml')
            table = so.find('table', class_='short')
            standings = table.findChild('tbody')
            teams_html = standings.findAll('tr')
            for team_ in teams_html:
                i = teams_html.index(team_)
                team = team_.text
                team_parts = team.split('\n')
                for part in team_parts:
                    if part == '' or part is None:
                        del(team_parts[team_parts.index(part)])
                team_data = {'league' : league, 'name': team_parts[1], 'position': team_parts[0], 'played': team_parts[6], 'wins': team_parts[7], 'draws': team_parts[8], 'losses': team_parts[9], 'goals scored': team_parts[16], 'goals against': team_parts[17], 'goal difference': team_parts[19], 'points': team_parts[20]}
                teams.append(team_data)
        if desired_team:
            desired = desired_team.replace('+', ' ')
            for team in teams:
                if team['name'].lower().find(desired.lower()) != -1:
                    break
            next_match = self.next_game_for(team)
            return next_match
        return teams

    def next_game_for(self, team):
        ret = {}

        if team['league'].find('Premier League') == -1:
            ret['error'] = 'Match day data only available for English Premiere League teams'
            return ret

        URL = 'http://api.football-data.org/soccerseasons/398/fixtures'
        response = requests.get(URL, headers={'X-Response-Control':'minified'}).json()
        for fixture in response['fixtures']:
            if self.before_today(fixture):
                continue
            if fixture['awayTeamName'].lower() == team['name'].lower() or fixture['homeTeamName'].lower() == team['name'].lower():
                next_match = fixture
                ret['source'] = 'Data provided by football-data.org and goal.com'
                ret['home'] = next_match['homeTeamName']
                ret['away'] = next_match['awayTeamName']
                ret['time'] = next_match['date']
                return ret
        return None

    def before_today(self, fixture):
        today = datetime.datetime.today()
        raw_match_time = fixture['date']
        match_date = raw_match_time[:raw_match_time.index('T')]
        matchday = datetime.datetime.strptime(match_date, '%Y-%m-%d')
        return today < matchday


class Yelp:
    CONSUMER_KEY = Config.YELP_CONSUMER_KEY
    CONSUMER_SECRET = Config.YELP_CONSUMER_SECRET
    TOKEN = Config.YELP_TOKEN
    TOKEN_SECRET = Config.YELP_TOKEN_SECRET

    def GET(self, location):
        log('Yelp')
        params = {
            'location': location,
            'limit': 20
        }
        auth = OAuth1(Yelp.CONSUMER_KEY, Yelp.CONSUMER_SECRET, Yelp.TOKEN, Yelp.TOKEN_SECRET)
        response = requests.get('http://api.yelp.com/v2/search', params=params, auth=auth).json()

        ret = []
        for business in response['businesses']:
            instance = {}
            instance['name'] = business['name']
            instance['address'] = business['location']['display_address'][0]
            instance['phone number'] = business.get('display_phone', 'None provided, submit correction?')
            instance['Yelp(R) deal?'] = 'deals' in business
            instance['rating'] = business['rating']
            instance['review count'] = business['review_count']
            if is_html():
                instance['url'] = '<a href="{0}">{1}</a>'.format(business['url'], business['title'])
                del(instance['title'])
            else:
                instance['url'] = business['url']
            instance['copyright'] = 'Data provided, and copyrighted by Yelp Inc.'
            ret.append(instance)
        return compression(ret)


class Sales:
    def GET(self, base_amount, rate):
        log('Sales')
        base_amount = float(base_amount)
        rate = float(rate)
        if rate > 1:
            rate = rate / 100
        amount = base_amount * rate
        ret = {'Subtotal': base_amount, 'Tax Rate': rate, 'Total': amount}
        return compression(ret)


class Holiday:
    def GET(self):
        log('Holiday')
        today = datetime.date.today()
        subdivision = web.input().get('state', 'CA')
        us_holidays = holidays.UnitedStates(state=subdivision)
        are_we_off = us_holidays.get(today) is None
        ret = {'Date': today.strftime('%b %d %Y'), 'Day Off': are_we_off}
        if are_we_off:
            description = us_holidays.get(today)
            if description is not None:
                ret['Holiday'] = description
            else:
                ret['Holiday'] = 'A day ending in -y'
        return compression(ret)


class Feed:
    def GET(self):
        log('Feed')
        feed_url = web.input().get('q', 'http://www.prolificprogrammer.com/atom.xml')
        parsed = feedparser.parse(feed_url)
        if not parsed.entries:
            raise WebError('No entries found')
        output_format = web.input().get('format', 'json')
        ret = []
        if 'updated_parsed' in parsed.entries[0]:
            entries = sorted(parsed.entries, key=operator.attrgetter('updated_parsed'), reverse=True)
        else:
            entries = parsed.entries

        if output_format == 'json':
            for index in xrange(0, len(entries)):
                item = entries[index]
                feed_entry = {}
                try:
                    feed_entry['author'] = item.get('author', parsed.feed.author)
                except AttributeError:
                    pass

                feed_entry['date'] = item.get('updated', item.get('published'))

                if is_html():
                    feed_entry['source'] = '<a href="{0}" target="_blank">{1}</a>'.format(parsed.feed.link, parsed.feed.title)
                    soup = BeautifulSoup(item['summary'], 'lxml')
                    feed_entry['link'] = '<a href="{0}" target="_blank">{1}</a>'.format(item['link'], unicode(item['title']).encode('utf-8'))
                    del(item['title'])
                    links = soup.find_all('a')
                    for link in links:
                        if link.has_attr('href') and link.get('href').startswith('http'):
                            link['target'] = '_new'
                    summary = str(soup)
                else:
                    feed_entry['link'] = item['link']
                    feed_entry['title'] = item.get('title')
                    soup = BeautifulSoup(item['summary'], 'lxml')
                    summary = soup.text
                    feed_entry['source'] = parsed.feed.title

                feed_entry['content'] = summary
                ret.append(feed_entry)
        elif output_format == 'ics':
            cal = Calendar()
            cal.add('prodid', '//units.d8u.us feed to ical converter')
            cal.add('version', '2.0')
            for index in xrange(len(entries)):
                item = entries[index]
                event = Event()
                event.add('summary', item.get('summary', item['description']))
                start_struct_time = item.get('updated_parsed', item.get('published_parsed'))
                start_epoch = time.mktime(start_struct_time)
                start = datetime.datetime.fromtimestamp(start_epoch)
                event.add('dtstart', start)
                event.add('dtend', start)
                event.add('organizer', item.get('author', parsed.get('author')))
                event['uid'] = start_epoch
                cal.add_component(event)
            ret = cal.to_ical()
            web.header('Content-Type', 'application/vcalendar')
        return compression(ret)


class Apod:
    def GET(self):
        log('Apod')
        parsed = feedparser.parse('http://apod.nasa.gov/apod.rss')
        ret = []
        for entry in parsed.entries:
            item = {}
            days_back = int(parsed.entries.index(entry))
            entry_date = datetime.date.today() - datetime.timedelta(days=days_back)
            item['date'] = entry_date.isoformat()
            item['title'] = entry.title
            item['link'] = entry.link
            image_link = BeautifulSoup(entry.summary, 'lxml')
            image = image_link.img['src']
            item['image'] = image
            item['Copyright Holder'] = 'NASA'
	    if is_html():
	        item['title'] = '<a href="{0}">{1}</a>'.format(entry.link, entry.title)
  		item['image'] = '<img src="{0}" title="{1}">'.format(image, entry.title)
                del(item['link'])
            ret.append(item)
        return compression(ret)


class Recipe:
    def GET(self):
        ret = {}
        params = web.input()
        if 'q' not in params:
            raise Error, 'search parameter (q) required'
        params = {'_app_id': Config.YUMMLY_ID, '_app_key': Config.YUMLY_SECRET}

        web_response = requests.get('http://api.yummly.com/v1/api/recipes', params=params)

        if web_response.status_code == '409':
            raise Error, "Rate limit exceeded!"
        response = web_response.json()
        ret['attribution'] = response['attribution']

        ret['dish'] = []
        for match in reponse['matches']:
            attrs = match['attributes']
            recipe = {}
            recipe['categories'] = ', '.join([v for v in attr.itervalues()])
            recipe['ingredients'] = ', '.join([i for i in match['ingredients']])
            recipe['title'] = match['recipeName']
            recipe['id'] = match['id']
            ret['dish'].append(recipe)
        return compression(ret)
        

class Imap:
    def GET(self, username, servername):
        log('Imap')
        ret = []
        params = web.input()

        if 'password' not in params:
            return Error(compression(json.dumps({'error': 'Missing password'}, indent=4)))

        passwd = params['password']
        try:
            if not params.has_key('ssl'):
                server = imaplib.IMAP4_SSL(servername)
            else:
                server = imaplib.IMAP4(servername)
        except BaseException, e:
            return compression({'error': str(e)})

        rv, data = server.login(username, passwd)
        rv, data = server.list()
        rv, data = server.select('inbox')
        rv, data = server.search(None, 'ALL')
        for msg_number in data[0].split():
            entry = {}
            msg = server.fetch(msg_number, '(RFC822)')[1][0][1]

            message = email.message_from_string(msg)

            decoded_subject = message['Subject']
            entry['subject'] = decoded_subject.decode('utf-8').encode('ascii', errors='ignore')

            date_tuple = email.utils.parsedate_tz(message.get('Date', message.get('Received', None)))
            entry['date'] =  datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple)).strftime("%a, %d %b %Y %H:%M:%S")
            entry['body'] = ''
            if message.is_multipart():
                for payload in message.get_payload():
                    entry['body'] = entry['body'] + str(payload)
            else:
                entry['body'] = str(message.get_payload())
            ret.append(entry)

        server.close()

        # always compress, as output will be large
        web.input()['compression'] = True
        return compession(ret)


class Reddit:
    def GET(self, subreddit=''):
        log('Reddit')
        url = 'https://www.reddit.com/{0}.json'.format('r{0}'.format(subreddit))
        # spoof user-agent, per https://www.reddit.com/r/help/comments/2pvmea/too_many_requests_but_i_have_no_idea_why/
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'}

        ret = []
        response = requests.get(url, headers=headers).json()

        if 'error' in response:
            raise Error("Something went wrong, on reddit's end wait a few minutes and try again")

        for post_ in response['data']['children']:
            stanza = {}
            post = post_['data']
            stanza['subreddit'] = post.get('subreddit', 'frontpage')
            stanza['author'] = post.get('author', post.get('name'))
            date = post.get('edited')
            if not date:
                date = post.get('created_utc')
            stanza['date'] = datetime.datetime.fromtimestamp(date).strftime('%c')
            stanza['title'] = post.get('title')
            if 'permalink' in post and post['permalink'] != '':
                stanza['link'] = 'https://www.reddit.com{0}'.format(post.get('permalink').encode('utf-8'))
                if is_html():
                    stanza['title'] = '<a href="{0}">{1}</a>'.format(stanza['link'], stanza['title'])
                    del(stanza['title'])

            if 'selftext' in post:
                stanza['content'] = self.to_plain(post.get('selftext').encode('utf-8'))

            ret.append(stanza)

        return compression(ret)

    def to_plain(self, some_html_string):
        html = markdown(some_html_string)
        plaintext = ''.join(BeautifulSoup(html, 'lxml').findAll(text=True))
        return plaintext


class Maven:
    def GET(self):
        params = web.input()
        query = params['q']
        remote_params = {'wt': 'json', 'q': query}
        results = requests.get('http://search.maven.org/solrsearch/select', params=remote_params).json()
        ret = {'query': query, 'source': 'Maven Central', 'results': []}
        for result in results['response']['docs']:
            stanza = {}

            group = result['g']
            artifact = result['a']
            version = result['latestVersion']
            packaging = result['p']
            
            repository = result['repositoryId'].lower()

            if packaging == 'bundle':
                packaging = 'jar'

            url = 'http://central.maven.org/maven2/{0}/{1}/{2}/{1}-{2}.{3}'.format(group.replace('.', '/'), artifact.replace('.', '/'), version, packaging)
            stanza['url'] = url
            
            metadata = requests.head(url).headers
            stanza['size'] = metadata['Content-Length']
            stanza['maven'] = '<dependency><artifactId>{0}</artifactId><groupId>{1}</groupId><version>{2}</version></dependency>'.format(artifact, group, version, packaging)
            stanza['ivyInBuildXml'] = '<ivy:cachepath organisation="{0}" module="{1}" revision="{2}" pathid="lib.path.id" inline="true"/>'.format(group, artifact, version)
            stanza['ivyStandalone'] = '<dependency org="{0}" name="{1} rev="{2}"/>'.format(group, artifact, version)
            stanza['gradle'] = '{0};{1};{2}'.format(group, artifact, version)
            stanza['sbt'] = '"{0}" % "{1}" % "{2}"'.format(group, artifact, version)

            timestamp = long(result['timestamp'])/1000
            revision_date = datetime.datetime.fromtimestamp(timestamp).strftime('%c')
            stanza['updatedAt'] = revision_date

            ret['results'].append(stanza)
        return compression(ret)


class Facebook:
    def get_access_token_from_url(url):
        """
        Parse the access token from Facebook's response
        Args:
             uri: the facebook graph api oauth URI containing valid client_id,
             redirect_uri, client_secret, and auth_code arguements
        Returns:
             a string containing the access key
        """
        token = str(urlopen(url).read(), 'utf-8')
        return token.split('=')[1].split('&')[0]
    
    def GET(self):
        log('Facebook')
        APP_KEY = Config.FACEBOOK_APP_KEY
        APP_SECRET = Config.FACEBOOK_APP_SECRET
        redirect_uri = 'http://{0}/{1}'.format(web.ctx.host, web.ctx.fullpath)
        
        access_token = web.input().get('code', None)
        access_url = 'https://www.facebook.com/v2.5/dialog/oauth?client_id={0}&redirect_uri={1}&scope=user_posts,user_friends'.format(APP_KEY, redirect_uri)
        if access_token is None:
            raise web.seeother(access_url)
        
        graph = GraphAPI()
        response = graph.get(path='oauth/access_token', client_id=APP_KEY, client_secret=APP_SECRET, redirect_uri=redirect_uri, code=access_token)
        data_ = urlparse.parse_qs(response)
        self.access_token = data_['access_token'][0]
        graph = GraphAPI(data_['access_token'][0])
        op = web.input().get('op', 'home')

        if op == 'home':
            response = graph.get('/v2.5/me/feed')            
            data = response['data']

        elif op == 'mutual':
            # Get mutual friends' names as a list of json, ordered by number of mutual friends
            other = web.input().get('other')
            response = graph.get(path='/v2.5/{0}'.format(other), fields='context.fields%28mutual_friends%29')
            data = response
           
        output_format = web.input().get('format','json')
        if output_format == 'json':
            ret = self.json(data)
        elif output_format == 'rss':
            ret = self.rss(data)
        elif output_format == 'atom':
            ret = self.atom(data)

        return compression(ret)

    def json(self, data):
        ret = []
        for datum in data:
            update = {}
            if 'message' in datum:
                update['status'] = datum['message']
            if 'story' in datum:
                update['details'] = datum['story']

            if len(update.keys()) > 0:
                update['creation_time'] = datum['created_time'] or datum['updated_time']

            ret.append(update)
        return ret

    def xml(self, data):
        fg = FeedGenerator()
        fg.id('http://units.d8u.us/fb?code={0}'.format(self.access_code))
        fg.title('Your Facebook(R) homescreen entries, as rss')
        fg.author({'name': 'Hasan Diwan', 'eamil': 'hd1@jsc.d8u.us'})
        fg.link('http://units.d8u.us/fb?code={0}&format=atom'.format(self.access_code), rel='alternate')
        for entry in data:
            fe = fg.add_entry()
            fe.title = entry.get('message', 'Untitled post')
            fe.content = entry.get('story', 'No details')
            fe.published = entry['created_time']
        return fg

    def rss(self, data):
        feed = self.xml(data)
        return feed.rss_str(pretty=True)

    def atom(self, data):
        feed = self.xml(data)
        return feed.atom_str(pretty=True)


class Wikipedia:
    URL = 'http://en.wikipedia.org/w/api.php?action=parse&format=json&page='
    def GET(self, query):
        log('Wikipedia')
        request_url = '{0}{1}'.format(Wikipedia.URL, query)
        response = requests.get(request_url).json()['parse']
        text = response['text']['*']
        content = BeautifulSoup(text, 'lxml').text
        title = response['displaytitle']
        ret = {'title': response['displaytitle'], 'summary': content}
        return compression(ret)


class Random:
    def GET(self):
        log('Random')

        random_type = web.input().get('type', 'string')

        if random_type == 'string':
            ret = self.random_string()
        elif random_type == 'number':
            ret = self.random_number()
        return compression(ret)

    def random_choice(self):
        rng = random.SystemRandom()
        choice_set = string.ascii_uppercase + string.digits + string.ascii_lowercase
        ret = rng.choice(choice_set)
        return ret

    def random_string(self, length=15):
        length = web.input().get('length', length)
        random_string = ''.join(self.random_choice() for n in xrange(1, int(length)))

        ret = {}
        ret['string'] = random_string
        ret['length'] = length
        return ret

    def random_number(self):
        up = web.input().get('upper', 2**6)
        upper = int(up)

        low = web.input().get('lower', 1)
        lower = int(low)
        ret = {}
        
        ret['number'] = random.randint(lower, upper)
        ret['lowerBound'] = lower
        ret['upperBound'] = upper
        return ret


class Github:
    def GET(self, query):
        log('Github')
        params = web.input()
        if 'repo' not in params:
            raise Error('Repository must be specified with the repo parameter', status='422 Unprocessable Entity')
        if 'lang' not in params:
            language = ''
        else:
            language = 'language:{0}'.format(params.get('lang'))
        repo = 'repo:{0}'.format(params.get('repo'))
        where = '{0}+in:file'.format(query)
        query_to_github = ' '.join([query, language, repo])
        response = requests.get('https://api.github.com/search/code', params={'q': query_to_github}).json()
        ret = []
        if response.has_key('errors'):
            ret = {'error': response['errors'], 'query': 'https://api.github.com/search/code?q={0}'.format(query_to_github)}
            return compression(ret)
        for repo in response['items']:
            value = {'name': repo['name'], 'path': repo['path']}
            value['content'] = requests.get(repo['git_url']).json()
            if str(value['content']).find('API rate limit exceeded for') > -1:
                value.pop('content', None)
            else:
                value['content'] = requests.get(repo['git_url']).json()['url']

            location = 'https://github.com/{0}/raw/master/{1}'.format(repo['repository']['full_name'], value['path'])
            value['url'] = location
            if is_html():
                value['q'] = '<a href="{0}">{1}</a>'.format(location, repo['path'])
                del(value['path'])
            ret.append(value)
        return compression(ret)


class EBay(xml.sax.ContentHandler):
    URL = 'http://svcs.ebay.com/services/search/FindingService?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME={0}&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&keywords='.format(Config.EBAY_APPNAME)

    def GET(self, query):
        log('Ebay')
        parser = xml.sax.make_parser()
        parser.setContentHandler(self)
        ebay_response_string = requests.get('{0}{1}'.format(EBay.URL,query)).content
        ebay_response = StringIO.StringIO(ebay_response_string)
        parser.parse(ebay_response)
        ret = self.results
        return compression(ret)

    def startDocument(self):
        self.results = []
        self.element = self.title = self.date = self.price = self.link = self.location = ''
        self.result = {}

    def startElement(self, name, attrs):
        self.element = name

    def endElement(self, name):
        if name == 'item':
            self.results.append(self.result)
            self.result = {}

        elif name == 'viewItemURL':
            self.result['link'] = self.link
            self.link = ''

        elif name == 'title':
            self.result['title'] = self.title
            self.title = ''

        elif name == 'convertedCurrentPrice':
            self.result['price'] = self.price
            self.price = ''

        elif name == 'endTime':
            self.result['endTime'] = self.date
            self.date = ''

        elif name == 'location':
            self.result['location'] = self.location
            self.location = ''

    def characters(self, data):
        if self.element.lower() == 'title':
            self.title = self.title + data
        elif self.element == 'convertedCurrentPrice':
            self.price = self.price + data
        elif self.element.lower() == 'endtime':
            self.date = self.date + data
        elif self.element == 'viewItemURL':
            self.link = self.link + data
        elif self.element == 'location':
            self.location = self.location + data

class IATA:
    def GET(self, code):
        log('IATA')
        ret = None
        airports_raw_data = requests.get('https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat').content
        airports_data = StringIO.StringIO(airports_raw_data)
        reader = csv.reader(airports_data)
        for line in list(reader):
            if line[4] == code.upper() and line[4] is not None:
                ret = {'airport': line[1], 'city': line[2], 'country': line[3], 'latitude': line[6], 'longitude': line[7], 'timezone': line[11]}
        if ret is None:
            ret = {'error': 'Airport not found'}
        return compression(ret)


class Stock:
    # http://www.canbike.org/information-technology/yahoo-finance-url-download-to-a-csv-file.html
    BASE_URL = 'http://download.finance.yahoo.com/d/quotes.csv'
    SYMBOL_RESOLUTION_URL = 'http://d.yimg.com/autoc.finance.yahoo.com/autoc?&region=1&lang=en&callback=YAHOO.Finance.SymbolSuggest.ssCallback&query='

    def GET(self):
        log('Stock')
        params = web.input()
        sym = params.get('sym', params.get('s', 'GOOG'))
        params = {'s': sym, 'e': '.csv', 'f': 'sohgpvc1d1t1np2'}
        results_ = requests.get(Stock.BASE_URL, params=params).content
        reader = csv.reader(StringIO.StringIO(results_))
        
        latest = list(reader)[0]
        try:
            symbol = latest[0]
            opening_price = latest[1]
            high_price = latest[2]
            low_price = latest[3]
            closing_price = latest[4]
            volume = latest[5]
            change = latest[6]
            last_trade_on = '{0} {1}'.format(latest[7], latest[8])
            name = latest[9]
            percent_change = latest[10]
        except BaseException,e:
            raise Error(compression(str(e)))

        ret = {'Trade Time': last_trade_on, 'Percent Change': percent_change, 'Company': name, 'Open': opening_price, 'High': high_price, 'Low': low_price, 'Close': closing_price, 'Volume': volume, 'Change': change, 'Source': 'Yahoo(R)', 'Symbol': sym}
        return compression(ret)


class AES:
    def GET(self):
        params = web.input()

        try:
            to_encrypt = params['s']
        except KeyError, k:
            raise Error(compression('Parameter s gives the data to be encrypted'))

        ALPHABET = ''.join([i for i in string.letters,string.digits])
        BLOCK_SIZE = 32

        salt = ''.join(random.choice(ALPHABET) for i in range(16))
        mode = Aes.MODE_CBC
        cipher = Aes.new(salt)

        secret_src = 'https://www.random.org/strings/?num=32&len=1&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new'.format(BLOCK_SIZE)
        secret = requests.get(secret_src).content.replace('\n','')

        aes = Aes.AESCipher(secret)
        crypt = base64.standard_b64encode(aes.encrypt(self.pad(to_encrypt)))
        ret = {'RijndaelEncryptedString': crypt, 'PlainText': to_encrypt, 'secret': secret}
        return compression(ret)

    def EncodeAES(self, c, s):
        padded_string = pad(s)
        crypt = c.encrypt(padded_string)
        return base64.encodestring(crypt)

    def pad(self, s, PADDING='{', BLOCK_SIZE=16):
        return s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING


class Worldbank:
    def GET(self):
        params = web.input()
        ret = []
        dataid = params.get('datafeed')

        if dataid is None:
            ret = self.get_indicator()
        else:
            ret = self.get_data_for(dataid)

        return compression(ret)

    def get_indicator(self):
        ret = []
        indicators = wb.get_indicators()
        for source_ in indicators.iterrows():
            source = source_[1]
            stanza = {}
            stanza['id'] = source['id']
            stanza['full name'] = source['name']
            stanza['study source'] = source['source']
            stanza['instructions'] =  'To request this data, send a request to http://units.d8u.us/worldbank?datafeed={0}'.format(source['id'])
            ret.append(stanza)
        return ret

    def get_data_for(self, sourceid):
        ret = []

        today = datetime.datetime.today()
        current_year = today.year
        response = requests.get('http://api.worldbank.org/countries/all/indicators/{0}?date=2002%3A{1}&per_page=25000&format=json'.format(sourceid, current_year)).json()
        data = response[1]
        for series in data:
            stanza = {}
            stanza['series'] = sourceid
            stanza['source'] = 'The World Bank Group'
            stanza['country'] = series['country']['value']
            stanza['date'] = series['date']
            stanza['value'] = series['value']
            if stanza['value'] is not None:
                ret.append(stanza)
        return ret


class Summarize:
    VALID_TYPES = ['application/pdf', 'application/msword', 'text/html', 'text/plain']

    def doc_to_text(self, path):
        cmd = ['antiword', path]
        p = subprocess.Popen(cmd, stdout=PIPE)
        stdout, stderr = p.communicate()
        ret = stdout.decode('ascii', 'ignore')
        return ret


    def docx_to_text(self, path):
        document = opendocx(path)
        paratextlist = getdocumenttext(document)
        newparatextlist = []
        ret = '\n\n'.join([newparatextlist.append(paratext.encode("utf-8")) for paratext in paratextlist])
        return ret


    def pdf_to_text(self, path):
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        codec = 'utf-8'
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        fp = file(path, 'rb')
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        password = ""
        maxpages = 0
        caching = True
        pagenos=set()
        for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
            interpreter.process_page(page)
        fp.close()
        device.close()
        str = retstr.getvalue()
        retstr.close()
        return str


    def html_to_text(self, html_string):
        tag_re = re.compile('<[^>]+>', re.I| re.MULTILINE | re.UNICODE)
        text = tag_re.sub(html_string, '')
        text = text.replace('&quot;', '"').replace("&apos;", "'").replace('&lt;', '<').replace('&gt;', '>')
        return text


    def get_sentiment(self, text):
        blob = TextBlob(text)
        ret = {'polarity': blob.polarity, 'subjectivity': blob.subjectivity}
        return ret


    def _calculate_languages_ratios(self, text):
        languages_ratios = LanguageIdentifier.from_modelstring(model, norm_probs=True).classify(text)
        return languages_ratios


    def detect_language(self, text):
        """
        Calculate probability of given text to be written in several languages and
        return the highest scored.
        
        It uses a stopwords based approach, counting how many unique stopwords
        are seen in analyzed text.
        
        @param text: Text whose language want to be detected
        @type text: str
 
        @return: Most scored language guessed
        @rtype: str
        """
        
        ratios = self._calculate_languages_ratios(text)
        
        most_rated_language = max(ratios, key=ratios.get)
        
        return most_rated_language
    

    def get_language(self, text):
        ret = {}
        ret['language'] = self.detect_language(text)

        return ret
    

    def lexical_diversity(self, text):
        set_of_text = set(text)
        length_of_set = len(set_of_text)
        length_of_text = len(text)
        ret = length_of_set/length_of_text
        return float(ret)


    def is_supported_mime_type(self, url):
        with tempfile.NamedTemporaryFile(prefix='units', suffix='.summary') as outfile:
            web_response = requests.get(url)
            response = web_response.content
            outfile.write(response)

            f = magic.Magic(uncompress=True, mime=True)
            mime_type = f.from_file(outfile.name)
            if mime_type not in self.VALID_TYPES:
                ret = False
            else:
                ret = True
            content = outfile.read()

        return {'type': mime_type, 'supported?': ret, 'data': content}

    
    def GET(self):
        params = web.input()
        url = params.get('url', 'https://donnaedwardsforsenate.com/')
        
        # make sure url is of the supported type -- http and https only
        parts = urlparse.urlparse(url)
        if not parts.scheme.startswith('http'):
            raise compression(Error('{0} schema unsupported'.format(parts.scheme)))

        try:
            mime_info = self.is_supported_mime_type(url)
        except ValueError, e:
            return Error(compression({'Error': str(e)}))
        if not mime_info['supported?']:
            raise Error(compression('Unsupported mime type for summary'))
    
        ret = {}
        SENTENCES_COUNT = 4
        soup = BeautifulSoup(requests.get(url).content,'lxml')

        # eliminate javascript
        [s.extract() for s in soup('script')]
        # eliminate css
        [s.extract() for s in soup('style')]

        text = u''.join([p.prettify() for p in soup.find_all('p')])

        sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
        sentences = sent_detector.tokenize(text)
        phrases_count = len(sentences)

        if phrases_count < int(SENTENCES_COUNT):
            message = "Summary length is greater than original, aborting."
            raise Error(message, status=404.13)

        summary = self.summary(text)
        ret['summary'] = unicode(''.join(summary['customSummary']))

        ret['diversity'] = str(self.lexical_diversity(text))

        from nltk.tokenize import TreebankWordTokenizer
        words = TreebankWordTokenizer().tokenize(text)
        ret['wordCount'] = str(len(words))

        ret['language'] = 'English'
        ret['sentenceCount'] = str(phrases_count)
        
        ret['fleschReadingEase'] = str(textstat.flesch_reading_ease(text))

        ret['fleschKincaidGradeLevel'] = str(textstat.flesch_kincaid_grade(text))

        ret['automatedReadingIndex'] = str(textstat.automated_readability_index(text))

        ret['smogIndex'] = str(textstat.smog_index(text))
        
        ret['fogScale'] = str(textstat.gunning_fog(text))
        
        ret['source'] = 'Bird, Steven, Edward Loper and Ewan Klein (2009), Natural Language Processing with Python. O’Reilly Media Inc., and textstat (https://github.com/shivam5992/textstat).'

        return compression(ret)

    
    def summary(self, html):
        ret = {}
        summary = []

        text = BeautifulSoup(html, 'lxml').get_text()
        text = re.sub('https?://[^ ]+', '', text)
        tokenizer = nltk.tokenize.TextTilingTokenizer()
        paragraphs = tokenizer.tokenize(text)
        logging.fatal('{0} paragraphs found'.format(len(paragraphs)))

        phrase_tokenizer = nltk.tokenize.punkt.PunktSentenceTokenizer()
        for para in paragraphs:
            # select the longest sentence of each paragraph and add it to the summary
            longest_phrase_length = max([len(s) for s in phrase_tokenizer.tokenize(para)])

            # find the first phrase with that length
            longest_phrase = [s for s in phrase_tokenizer.tokenize(para) if len(s) == longest_phrase_length][0]
            summary.append(longest_phrase.replace('\n', '      '))

        ret['customSummary'] = [re.sub(' +', ' ', phrase) for phrase in summary]
        return ret


class Verify:
    def GET(self):
        params = web.input()
        guess = params['guess']
        real = params['real']
        result = guess == real
        ret = {'result': result }
        return compression(ret)


class Weather:
    FORECAST_API_KEY = Config.FORECAST_IO_API_KEY

    def GET(self, zipcode):
        log('Weather')
        zipcode_plus_escaped = zipcode.replace(' ', '+')
        RESOLVING_URL = 'http://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=false'.format(zipcode_plus_escaped)
        resolving_responses = requests.get(RESOLVING_URL).json()['results']
        resolving_response = resolving_responses[0]
        geometry = resolving_response['geometry']
        location = geometry['location']
        latitude = location['lat']
        longitude = location['lng']

        FORECAST_URL = 'https://api.forecast.io/forecast/{0}/{1},{2}'.format(Weather.FORECAST_API_KEY, latitude, longitude)
        forecast_response = requests.get(FORECAST_URL).json()
        current = forecast_response['currently']
        ret = {}
        ret['source'] = 'Forecast.io'
        ret['location'] = zipcode
        ret['epochTime'] = current['time']

        cutoff = str(current['time']).find('.')-1
        ret['timeUTC'] = datetime.datetime.fromtimestamp(float(current['time'])).strftime('%c')
        ret['summary'] = current['summary']
        ret['nearestStormDistance'] = current['nearestStormDistance']
        if 'nearestStormBearing' in current:
            ret['nearestStormBearing'] = current['nearestStormBearing']
        ret['precipitationChance'] = current['precipProbability']
        if 'precipType' in current:
            ret['precipitationType'] = current['precipType']
        ret['temperatureFahrenheit'] = current['temperature']
        ret['apparentTemperatureFahrenheit'] = current['apparentTemperature']
        celcius = requests.get('http://units.d8u.us/unit/tempF/tempC/{0}'.format(current['temperature'])).json()['amountDesired']
        ret['temperatureCentigrade'] = round(float(celcius), 2)
        celcius = requests.get('http://units.d8u.us/unit/tempF/tempC/{0}'.format(current['apparentTemperature'])).json()['amountDesired']
        ret['apparentTemperatureCentigrade'] = round(float(celcius), 2)
        ret['windSpeed'] = current['windSpeed']
        ret['windBearing'] = current['windBearing']
        ret['cloudCover'] = current['cloudCover']
        ret['humidity'] = current['humidity']
        ret['barometricPressure'] = current['pressure']
        ret['visibility'] = current['visibility']
        ret['ozone'] = current['ozone']

        if is_html():
            ret['temperatureFahrenheit'] = '{0}&#8457;'.format(ret['temperatureFahrenheit'])
            ret['temperatureCentigrade'] = '{0}&#8457;'.format(ret['temperatureCentigrade'])
            ret['apparentTemperatureFahrenheit'] = '{0}&#8457;'.format(ret['apparentTemperatureFahrenheit'])
            ret['apparentTemperatureCentigrade'] = '{0}&#8457;'.format(ret['apparentTemperatureCentigrade'])
            ret['humidity'] = '{0}%'.format(ret['humidity'])
            ret['windBearing'] = '{0}&#176;'.format(ret['windBearing'])
            ret['precipitationChance'] = '{0}%'.format(float(ret['precipitationChance']) * 100)
            ret['windSpeed'] = '{0} mph'.format(ret['windSpeed'])
            ret['cloudCover'] = '{0}%'.format(float(ret['cloudCover'] * 100))
            ret['source'] = '<a href="{0}">{1}</a>'.format(FORECAST_URL, ret['source'])
            if 'nearestStormBearing' in ret:
                ret['nearestStormBearing'] = '{0}&#176;'.format(ret['nearestStormBearing'])

        return compression(ret)


class PhoneTime:
    def GET(self, number):
        if not number.startswith('+'): 
            number = '+1{0}'.format(number)

        if number.startswith('+1'):
            area_code = number[2:5]
            data = requests.get('http://www.areacodetimezone.com/areacode/area-code-{0}.html'.format(area_code)).content
            
            soup = BeautifulSoup(data, 'lxml')
            h2 = soup.find_all('h2')[2]
            full_time = h2.text
            end_marker = full_time.index('m')+1
            current_time = full_time[:end_marker]

        else:
            number = number[1:] # drop the plus
            # read in codes into python
            data = requests.get('https://gist.github.com/adhipg/1600028/raw/a25f6d472659efd4cd5f790da253a758be5833e6/countries.sql', stream=True).content
            # massage data
            country_data = data[data.index('INSERT INTO `country` ')+len('INSERT INTO `country` '):]
            countries = country_data.replace('(','').replace('),','').replace('VALUES','')[:-1].replace(')','').replace('`','')[:-1]
            countries_io = StringIO.StringIO(countries)
            countries_csv = csv.DictReader(countries_io)
            countries_csv.fieldnames = [f.strip() for f in countries_csv.fieldnames]
            
            # Finally, get it in a python data structure
            countries_list = list(countries_csv)
            for country in countries_list:
                phone_code = country['phonecode'].strip()
                if number.startswith(phone_code):
                    country_name = country['iso']
                    break
                
            tz_zip_location = 'https://timezonedb.com/files/timezonedb.csv.zip'
            tz_zip_content = requests.get(tz_zip_location, stream=True).content
            tz_zip_stream = StringIO.StringIO(tz_zip_content)
            with zipfile.ZipFile(tz_zip_stream) as timezones_source:
                zone_name = ''
                zones = timezones_source.read('zone.csv')
                zones_csv = zones.split('\n')
                for zone_csv in zones_csv:
                    zone = zone_csv.split(',')
                    country_candid8 = zone[1].replace('"', '')
                    country_name = country['iso'].strip().replace("'", '')
                    if country_name == country_candid8:
                        zone_name = zone[2][1:-1]
                        break

            data = requests.get('http://www.epochconverter.com/epoch/timezones.php', stream=True).content
            soup = BeautifulSoup(data, 'lxml')
            
            table_rows = soup.find_all('tr')
            for row in table_rows:
                row_data = row.find_all('td')
                for element in row_data:
                    text = element.text
                    if text.find(zone_name) != -1:
                        current_time = element.find_next().text
                        break
        try:
            ret = {'Time': current_time}
        except UnboundLocalError, e:
            raise Error('{0} invalid'.format(number), status=422)

        return compression(ret)


class Time:
    def GET(self, current_offset, desired_offset, now=datetime.datetime.now()):
        log('Time')
        try:
            desired = pytz.timezone(desired_offset)
        except:
            ret = {'Error': "unknown timezone {0}".format(current_offset), "Valid": pytz.all_timezones}
            return compression(ret)
        try:
            current = pytz.timezone(current_offset)
        except:
            ret = {'Error': "unknown timezone {0}".format(current_offset), 'Valid': pytz.all_timezones}
            return Error(compression(ret), status=422)

        try:
            time = datetime.datetime.strptime(now, '%Y%m%d%H%M')
        except ValueError:
            try:
                time = datetime.datetime.strptime(now, '%Y%m%d%H%M%S')
            except BaseException:
                time = datetime.datetime.now()
        offset = current.utcoffset(time)
        tagged_time = current.localize(time + offset)
        result = tagged_time.astimezone(desired)
        ret = {'desiredTimezone': desired_offset, 'currentTimezone': current_offset, 'currentTime': tagged_time.strftime('%c'), 'desiredTime': result.strftime('%c')}
        return compression(ret)


class Unit:
    def GET(self, unit_from, unit_to, amount=None):
        log('Unit')
        if amount is None:
            amount = 1

        if self.is_currency(unit_from) and self.is_currency(unit_to):
            ret = self.currency(unit_from, unit_to, amount)
        else:
            try:
                if unit_from == 'degC': 
                    unit_from = 'tempC'
                if unit_from == 'degF':
                    unit_from = 'tempF'
                if unit_to == 'degC':
                    unit_to = 'tempC'
                if unit_to == 'degF':
                    unit_to = 'tempF'

                ret = self.unit(unit_from, unit_to, amount)
            except BaseException, e:
                ret = {'error': 'Unit conversion failed: {0}'.format(str(e))}

        return compression(ret)

    # conversion type validation
    def is_currency(self, unit):
        is_uppercase = unit.upper() == unit
        has_valid_length = len(unit) == 3
        return is_uppercase and has_valid_length


    def unit(self, unit_from, unit_to, amount):
        if not unit_from.startswith('temp'):
            arg1 = '{0} {1}'.format(amount, unit_from)
        else:
            arg1 = '{1}({0})'.format(amount, unit_from)
        arg2 = unit_to
        cmd = '/usr/local/bin/gunits'
        args = [cmd, arg1, arg2]
        output = subprocess.check_output(args)
        lines = output.split('\n')
        line = lines[0]
        value = line.strip().replace(r'* ', '').strip()
        ret = {'amountIn': amount, 'unitIn': unit_from, 'amountDesired': value, 'unitDesired': unit_to}
        return ret


    def currency(self, unit_from, unit_to, amount):
        FIRST = 'http://free.currencyconverterapi.com/api/v3/convert?q={0}_{1}&compact=y'.format(unit_from,unit_to)
        resp = requests.get(FIRST).json()
        quotes = resp['{0}_{1}'.format(unit_from,unit_to)]
        query = quotes['val']

        value = float(query)*float(amount)
        value = '{:.2f}'.format(value)
        ret = {'amountIn': amount, 'unitIn': unit_from, 'amountDesired': value, 'unitDesired': unit_to, 'Source': 'CurrencyLayer'}
        return ret


class SEO:
    def GET(self):
        body = {}
        body['ajax'] = 1
        body['geo'] = web.input().get('place','US')
        body['htd'] = datetime.date.today().strftime('%Y%m%d')
        body['pn'] = 'p1'
        body['htv'] = 'l'
        response = requests.post('http://www.google.com/trends/hottrends/hotItems', json=body).json()
        return compression(response.keys)


class Location:
    def GET(self):
        log('Location')
        ret = []

        if web.input().has_key('location'):
            results = self.forward()
        elif ['latitude', 'longitude'] in web.input().iterkeys() is True and not web.input().has_key('location'):
            results = self.reverse()
        else:
            raise Error('Need parameter location or latitude/longitude must be specified for geocoding')

        for result in results['results']:
            item = {}
            item['formatted'] = result['formatted']
            item['url'] = result['annotations']['OSM']['url']
            ret.append(item)

        return compression(ret)

    def forward(self):
        ret = []
        location = web.input()['location']

        params = {}
        params['q'] = location
        params['key'] = Config.OPENCAGE_KEY

        ret = requests.get(Config.OPENCAGE_ENDPOINT, params=params).json()
        return ret

    def reverse(self):
        latitude = web.input()['latitude']
        longitude = web.input()['longitude']
        
        params = {}
        params['q'] = '{0}+{1}'.format(latitude, longitude)
        params['key'] = Config.OPENCAGE_KEY
        
        response = requests.get(Config.OPENCAGE_ENDPOINT, params=params).json()
        return response


class Package:
    # Add intelligence here
    def guess_carrier(self, tracking_number):
        params = {'tracking_number': tracking_number}

        header = self.get_header()
        web_response = requests.post('https://api.aftership.com/v4/tracking/couriers/detect', params=params, verify=False, headers=header)
        response = web_response.json()
        
        carrier_slug = response['slug']
        if type(carrier_slug) == types.ListType:
            carrier_slug = carrier_slug[0]

        return carrier_slug

    def get_header(self):
        header = {}
        header['aftership-api-key'] = Config.AFTERSHIP_KEY
        header['Content-Type'] = 'application/json'
        return header

    def GET(self, tracking_number):
        ret = {}

        params = web.input()
        package_type = params.get('carrier') #, self.guess_carrier(tracking_number))
        
        header = self.get_header()
        web_response = requests.get('https://api.aftership.com/v4/trackings/{0}/{1}'.format(package_type, tracking_number), headers=header, verify=False)

        response = web_response.json()

        try:
            ret['delivery'] = response['expected_delivery']
        
            weight = response['shipment_weight']
            weight_unit = response['shipment_weight_unit']
            web_response = requests.get('http://units.d8u.us/unit/{0}/grams/{1}'.format(weight_unit, weight))
            response = web_response.json()
            ret['weight'] = '{0} grams'.format(response['amountDesired'])
            
            ret['trackingNumber'] = tracking_number
            
            checkpoints = response['checkpoints']
            last_checkpoint = checkpoints[-1]
            
            ret['status'] = last_checkpoint['tag']
            ret['time'] = last_checkpoint['checkpoint_time']
        except KeyError, e:
            pass

        return compression(ret)


class PGP:
    def POST(self, *args, **kwargs):
        ret = ''
        keys = json.loads(self.GET(*args, **kwargs))
        
        if keys.has_key('error'):
            raise Error(compression(ret))
        
        key_id = keys['key_id'][2:]

        body = web.data()
        
        params = web.input()
        passphrase = params['passphrase']
        ret = subprocess.call(['/usr/local/bin/gpg', '--keyserver', 'keyserver.ubuntu.com', '--recv-key', key_id])
        cmd = ['/usr/local/bin/gpg', '--passphrase', passphrase, '--armor', '--encrypt', '--yes', '--batch', '--trust-model', 'always', '--recipient', key_id, '--local-user', '0xFEBAD7FFD041BBA1']
        ret = subprocess.check_output(cmd, shell=True)
        # subprocess.call(['/bin/rm', '-f', '/home/ec2-user/.gnupg/pubring.gpg'])

        return compression(ret)
        
    def GET(self, *args, **kwargs):
        ret = None

        fp_regex = r'Fingerprint=(.*) '
        fingerprint_reg = re.compile(fp_regex)
        
        keyid_regex = '/([A-F0-9]+)'
        keyid_reg = re.compile(keyid_regex)

        expiration_regex = '([0-9]{4}-[0-9]{2}-[0-9]{2})'
        expiration_reg = re.compile(expiration_regex)

        length_regex = '([0-9]+)R/'
        length_reg = re.compile(length_regex)

        params = web.input()

        key_id = params.get('q')
        if not key_id:
            raise Error("Must have parameter q corresponding to a substring matching a key's id or name or email")

        ret = []

        params['search'] = key_id
        params['op'] = 'index'
        params['fingerprint'] = 'on'

        web_response = requests.get('http://pgp.zdv.uni-mainz.de:11371/pks/lookup', params=params)
        response = web_response.content
        soup = BeautifulSoup(response, 'lxml')
        items = soup.find_all('pre')[1:]

        if len(items) < 1:
            if not key_id.startswith('0x'):
                key_id = '0x{0}'.format(key_id)
            params['search'] = key_id
            web_response = requests.get('http://pgp.zdv.uni-mainz.de:11371/pks/lookup', params=params)
            response = web_response.content
            soup = BeautifulSoup(response, 'lxml')
            items = soup.find_all('pre')[1:]

        if len(items) < 1:
            ret = {'error': '{0} not found'.format(params['q'])}
            raise Error(json.dumps(ret))

        for key in items:
            text = key.text
            matched = fingerprint_reg.search(text)
            keyinfo = {}
            keyinfo['fingerprint'] = matched.group(1)

            matched = keyid_reg.search(text)
            keyinfo['key_id'] = '0x{0}'.format(matched.group(1))

            matched = expiration_reg.search(text)
            keyinfo['expiration'] = expiration = matched.group(1)

            gecos_start = text.index(expiration)+len(expiration)+1
            gecos_end = text[2:].index('\n')+1
            keyinfo['user'] = text[gecos_start:gecos_end]

            length_and_post = text[5:].strip()
            length = length_and_post[:length_and_post.index('/')-1]
            keyinfo['length'] = length

            ret.append(keyinfo)
        
        if len(ret) == 1:
            ret = ret[0]
        
        return compression(ret)


class Yodlee:
    def valid_name(self, supported):
        return web.input()['bank'] in supported['name']

    def valid_yodlee_bank_id(self, institute):
        return all([i in string.digits for i in institute])

    def lookup_bank(self, name):
        ret = []
        params = {}
        params['rn'] = name

        data = pd.read_csv('/var/tmp/routing_numbers.tsv.xz', delimiter='\t')
        search_string = name.upper()
        for line in data:
            #logging.fatal(line)
            if search_string in line:
                (routing, telegraphic_name, full_name, state, city, funds_transfer_status, settlement_status, securities_transfer_status, revision_date) = (line[0:9].strip(), line[9:26].strip(), line[26:62].strip(), line[63:64].strip(), line[64:89].strip(), line[90].strip(), line[91].strip(), line[92].strip(), line[93:].strip())
                stanza = {}
                stanza['routingNumber'] = routing
                stanza['bankName'] = full_name
                stanza['location'] = '{1}, {0}, USA'.format(city, state)
                ret.append(stanza)
                
        return ret

    def GET(self):
        ret = []
        if web.input().has_key('refresh'):
            try:
                collect_yodlee()
                ret = {'Status': 'Refresh initiated at {0}'.format(datetime.datetime.now())}
            except BaseException, e:
                raise Error(compression(str(e)))

            return compression(ret)

        with gzip.open(Config.YODLEE_PROVIDERS_PATH) as fin:
            institutions_json = fin.read()
            try:
                institutions = json.loads(institutions_json)
            except BaseException, e:
                raise Error(compression(str(e)))

        if not web.input().has_key('bank'):
            return compression(institutions)

        if web.input().has_key('bank') and not web.input().has_key('session'):
            bank =  web.input()['bank']
            search_query = str(bank).lower()
            for r in institutions:
                try:
                    institution = r['name']
                except TypeError:
                    # something up with Yodlee, some institutions don't have a name?
                    continue
                institution_search = institution.lower()
                
                if institution_search.find(str(search_query)) != -1:
                    r['aba'] = self.lookup_bank(r['name'])
                    ret.append(r)

            if len(ret) > 1:
                return compression(ret)

            for r in institutions:
                institution_search = r['id']
                ret.append(r)
                try:
                    search_query = int(bank)
                except ValueError, e:
                    pass
                    
                if institution_search == search_query:
                    break

            ret = ret[0]

            params = {}
            params['cobrandLogin'] = Config.YODLEE_COBLOGIN
            params['cobrandPassword'] = Config.YODLEE_COBPASSWORD
            response_src = requests.post('{0}/cobrand/login'.format(Config.YODLEE_ENDPOINT), verify=False, params=params)
            response = response_src.json()
        
            logging.info('From {1}: {0}'.format(response, response_src.url))
            session = response['session']
            cobrand = session['cobSession']
            logging.info('cobSession: {0}'.format(cobrand))

            params = {}
            params['loginName'] = Config.YODLEE_LOGIN
            params['password'] = Config.YODLEE_PASSWORD
            headers = {}
            headers['Authorization'] = '{cobSession='+cobrand+'}'

            web_response = requests.post('{0}/user/login'.format(Config.YODLEE_ENDPOINT), headers=headers, verify=False, params=params)
            logging.info('{0} is the response from logging in as {1} on {2}'.format(web_response.status_code, Config.YODLEE_LOGIN, Config.YODLEE_ENDPOINT))

            response = web_response.json()
            user = response['user']
            session = user['session']
            user_session = session['userSession']
            logging.info('{0} is our user session'.format(user_session))

            headers['Authorization'] = '{cobSession='+cobrand+',userSession='+user_session+'}'
            self.persist(headers)

            institution = ret

            params['container'] = 'bank'
            providerId = institution['id']
            response = requests.get('{0}/providers/{1}'.format(Config.YODLEE_ENDPOINT, providerId), headers=headers, verify=False).json()
            logging.info('GET /providers/{1} response is {0}'.format(response, providerId))
            
            provider_stanza = response['provider'][0]
            logging.info('{0} is the provider stanza'.format(provider_stanza))
            
            form_stanza = provider_stanza['loginForm']
            logging.info('{0} is the provider login form'.format(form_stanza))
            
            form_fields = form_stanza['row']
            number_of_fields = len(form_fields)
            our_form_fields = form_fields
            fields = []

            for form_field_location in xrange(0, len(form_fields)):
                form_field = our_form_fields[form_field_location]
                our_fields = form_field['field']
                for field in our_fields:
                    position = our_fields.index(field)
                    fields.append(field)
                    field_name = field['name']
                    logging.info('Field: {0}'.format(field))
                    if not web.input().has_key(field_name):
                        if not ret.has_key('missing'):
                            ret['missing'] = []
                        ret['missing'].append(field_name)
                    else:
                        value = web.input()[field_name]
                        fields[-1]['value'] = value
                    
                    our_fields[position] = fields[-1]                

            if ret.has_key('missing'):
                return compression(ret)


            provider_stanza['loginForm'] = form_stanza
            provider = {'provider': [provider_stanza]}

            logging.info('Provider filled-in fields: {0}'.format(json.dumps(provider)))
            web_response = requests.post('{0}/providers/{1}'.format(Config.YODLEE_ENDPOINT, providerId), headers=headers, json=provider, verify=False)
            response = web_response.json()
            params['accountIds'] = response['providerAccountId']

        status_stanza = requests.get('{0}/refresh'.format(Config.YODLEE_ENDPOINT), headers=headers, params=params, verify=False).json()
        try:
            refresh_info = status_stanza['refreshInfo']
        except KeyError, e:
            message = status_stanza['errorMessage']
            raise Error(message)

        status = refresh_info['status']

        while status != 'REFRESH_COMPLETED':
            logging.debug('In refresh loop')
            status_stanza = requests.get('{0}/refresh'.format(Config.YODLEE_ENDPOINT), headers=headers, params=params, verify=False).json()['refreshInfo']['status']
        
        params['status'] = 'ACTIVE'
        params['providerAccountId'] = account_id
        
        web_response = requests.get('{0}/accounts'.format(Config.YODLEE_ENDPOINT), headers=headers, params=params, verify=False)
        response = web_response.content
        logging.debug('{0} yields {1}'.format(web_response.url, response))
        
        account = response['account']
        
        available_balance = response['availableBalance']
        balance = self.convert(available_balance)
        usd_balance = balance['amount']
        account_id = account['accountNumber']
        account_name = account['accountName']
        
        if not ret.has_key('accounts'):
            ret['accounts'] = {}
            
            our_account = {'name': account_name, 'balance': usd_balance}
            ret['accounts'] = {accound_id: our_account}
        
        else:
            ret['institutions'] = institutions

        return compression(ret)


    def persist(self, headers):
        try:
            [self.auth.append(h) for h in headers]
        except AttributeError, e:
            self.auth = headers
        return True


    def convert(self, balance):
        currency = balance['currency']
        if currency != 'USD':
            amount = balance['amount']
            conversion_site_url = 'http://units.d8u.us/unit/{0}/USD/{1}'.format(currency, amount)
            response = requests.get(conversion_site_url, verify=False).json()

            balance['currency'] = 'USD'
            balance['amount'] = response['amountDesired']
        return balance


class Fips:
    def GET(self, state=None, county=None):
        parameters = web.input()
        good_state = parameters.get('state', state)
        good_county = parameters.get('county', county)
        
        ret = pd.read_excel('http://www.stssamples.com/xls/US_FIPS_CountyCodes.xls')
        
        ret = [state for state in good_state if state.lower().startswith(good_state.lower())]
        if good_county is not None:
            filtered = [row['County Name'] for row in good_county if row['County Name'].startswith(row['County Name'])]
            ret = filtered[0]
            for r in ret.iterkeys():
                if type(ret[r]) == types.StringType:
                    logging.debug('Found string!')
                    ret[r] = ret[r].strip()
        return compression(ret)


class Dwolla:
    def get_balance(self, params):
        from dwolla import accounts

        account_number = params['account']
        ret = accounts.basic(account_number)

        logging.debug('Returning {0}'.format(json.dumps(ret, indent=1)))
        return ret


    def pay(self, params):
        from dwolla import request
        account = params['account']
        amount = params['amount']
        
        ret = request.create(account, amount)
        logging.debug('Request created: {0}'.format(json.dumps(ret, indent=1)))
        return ret


    def GET(self):
        from dwolla import constants
        parameters = web.input()
        constants.client_id = config.DWOLLA_API_KEY
        constants.client_secret = config.DWOLLA_API_SECRET

        op = parameters.get('op', 'balance')
        if op == 'balance':
            ret = self.get_balance(parameters)
        elif op == 'pay':
            ret = self.pay(parameters)

        return compressed(ret)


class Help:
    def GET(self):
        log('Help')
        logging.debug('in help endpoint')
        ret = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Supported interactions</title><body><table><thead><tr><th>Endpoint</th><th>Classname</th></tr><tbody>'
        for url in xrange(0,len(urls),2):
            ret = ret + '<tr><td>{0}</td><td>{1}</td></tr>'.format(urls[url], urls[url+1])
        ret = ret + '</tbody></table></body></html>'
        return compression(ret)


def collect_yodlee():
    with gzip.open(Config.YODLEE_PROVIDERS_PATH, 'wb') as fout:
        params = {}
        params['cobrandLogin'] = Config.YODLEE_COBLOGIN
        params['cobrandPassword'] = Config.YODLEE_COBPASSWORD
        login_body = ''
        for param in params.iterkeys():
            login_body = login_body + '{0}={1}&'.format(param, params[param])
        login_body = login_body[:-1]

        responseobj = requests.post('{0}/cobrand/login'.format(Config.YODLEE_ENDPOINT), params=login_body, verify=False)
        response = responseobj.json()
        cobrand = response['session']['cobSession']

        headers = {}
        headers['Authorization'] = '{cobSession='+cobrand+'}'
        getparams = {}
        getparams['loginName'] = Config.YODLEE_LOGIN
        getparams['password'] = Config.YODLEE_PASSWORD
        web_response = requests.post('{0}/user/login'.format(Config.YODLEE_ENDPOINT), params=getparams, headers=headers, verify=False)

        response = web_response.json()

        user_session = response['user']['session']['userSession']
        headers['Authorization'] = '{cobSession='+cobrand+',userSession='+user_session+'}'

        response = requests.get('{0}/providers'.format(Config.YODLEE_ENDPOINT), params=params, headers=headers, verify=False).json()
        institutions = []
        for institution in response['provider']:
            institute = {}
            institute['id'] = institution['id']
            institute['name'] = institution['name']

            if institution.has_key('loginUrl'):
                institute['login'] = institution['loginUrl']

            if institution.has_key('baseUrl'):
                institute['url'] = institution['baseUrl']

            institutions.append(institute)
            
        while len(institutions) % 500 == 0: 
            params['skip'] = len(institutions)

            response = requests.get('{0}/providers'.format(Config.YODLEE_ENDPOINT), params=params, headers=headers, verify=False).json()
            
            for institution in response['provider']:
                institute = {}
                institute['id'] = institution['id']
                institute['name'] = institution['name']
                if institution.has_key('baseUrl'):
                    institute['url'] = institution['baseUrl']
                institutions.append(institute)
        fout.write(json.dumps(institutions))


def main(ip_address='127.0.0.1:7860'):
    sys.argv.append(ip_address)
    web.config.debug = False
    app.run()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)5d %(pathname)s %(lineno)d %(levelname)s %(message)s')
    t=threading.Thread(target=collect_yodlee, name='Yodlee-provider-collect', verbose=1)
    t.start()

    main()
