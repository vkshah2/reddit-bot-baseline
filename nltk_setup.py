import os
import logging
import urlparse

from bs4 import BeautifulSoup, SoupStrainer
import requests

NLTK_DATA_ROOT = '/usr/share/nltk_data'
logging.basicConfig(level=logging.DEBUG)

try:
    os.mkdir(NLTK_DATA_ROOT)
except OSError, e:
    logging.debug(str(e))
    if str(e).find('13') != -1:
        message = 'Permission denied, must run as root or under sudo'
        logging.fatal(message)
        raise SystemError, message
    elif str(e).find('17') != -1:
        pass

data = requests.get('http://www.nltk.org/nltk_data/').content

soup = BeautifulSoup(data, 'lxml')
packages = soup.find_all('package', {'url': True})

for package in packages:
    url = package.get('url')
    parsed = urlparse.urlparse(url)
    path = parsed.path
    nltk_relative_path = path[path.index('packages/')+9:]
    relative_path = '{0}/{1}'.format(NLTK_DATA_ROOT, nltk_relative_path)
    directory = relative_path[:relative_path.rfind('/')]
    filename = relative_path[relative_path.rfind('/')+1:]
    filepath = os.path.join(NLTK_DATA_ROOT, directory, filename)

    try:
        os.mkdir(directory)
    except OSError, e:
        pass

    stream = requests.get(url, stream=True)
    with open(filepath, 'wb') as fout:
        logging.debug('Writing {0} to {1}'.format(stream.url, filename))
        [fout.write(chunk) for chunk in stream.iter_content(chunk_size=1024) if chunk]
